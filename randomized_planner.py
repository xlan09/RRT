#!/usr/bin/env python3
"""
RRT algorithm
"""

from environment import World
from plotter import Plotter
from enum import Enum
import random
import abc
import numpy as np


class PlannerType(Enum):
    """
    Enum to denote planner types supported
    """
    rrt = 1


class Planner(object):
    """
    Planner
    """

    # only consider planning in this environment
    world = World([0, 0], [100, 100])

    @classmethod
    def plan(cls, start_pos: list, goal_pos: list, planner_type: PlannerType):
        if not cls._is_pos_valid(start_pos, goal_pos):
            raise ValueError('Please choose free points in the world ' + np.array_str(np.array(start_pos)) + ',  ' \
                                  + np.array_str(np.array(goal_pos)) + ' as start pos and goal pos.' )

        if planner_type is PlannerType.rrt:
            planner = RRT(cls.world)
        else:
            raise TypeError('Not supported planner type')

        return planner.start_planning(start_pos, goal_pos)

    @classmethod
    def _is_pos_valid(cls, start_pos, goal_pos):
        """
        Make sure start pos and goal pos are in the free space of the world
        """

        if cls.world.is_free(start_pos) and cls.world.is_free(goal_pos):
            return True
        else:
            return False


class TreeNode(object):
    """
    Tree Node
    """

    def __init__(self, pos: list):
        self.pos = np.array(pos)
        self.children = []

        # used for Kd tree
        self.left = None
        self.right = None
        self.splitDim = 0

    def dist2other(self, other_node):
        """
        Calculate distance to other node
        """

        return np.linalg.norm(self.pos - other_node.pos)


class RandomTree(object):
    """
    Randomized tree
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, world: World):
        self.root = None
        self.world = world
        self.nodes = []
        self.kdtree = KdTree()
        self.seg_len = 5 # largest distance betweent two nodes

    @abc.abstractmethod
    def start_planning(self, start_pos, goal_pos):
        raise NotImplementedError('start_planning is abstract method')

    @abc.abstractmethod
    def _extend(self):
        raise NotImplementedError('extend is abstract method')

    def _generate_random_node(self, world: World):
        while 1:
            rand_pos = np.array([random.uniform(world.sw_corner[0], world.ne_corner[0]) , random.uniform(world.sw_corner[1], world.ne_corner[1])])
            if world.is_free(rand_pos):
                break

        return TreeNode(rand_pos)

    def _get_node_to_add(self, seg_len):
        """
        Find a node between the nearest node and the new node generated
        """
        while 1:
            new_node = self._generate_random_node(self.world)
            nearest_node = self._nearest_neighbor(new_node)
            node_to_add = TreeNode(nearest_node.pos + (new_node.pos - nearest_node.pos) / np.linalg.norm(new_node.pos - nearest_node.pos) * seg_len)
            if not PlanningUtil.is_collision(node_to_add, nearest_node, self.world):
                break

        return node_to_add, nearest_node

    def _nearest_neighbor(self, query_node: TreeNode):
        """
        Find the nearest neighbor inside the tree
        """

        return self.kdtree.nearest_neighbor(query_node)

    def _add_node_to_tree(self, new_node: TreeNode):
        """
        Add a d new node to tree
        """

        if new_node is None:
            raise ValueError('node added to tree can not be null')

        self.nodes.append(new_node)
        self.kdtree.add_to_tree(new_node)

    def _init_tree(self, start_pos):
        self.root = TreeNode(start_pos)
        self._add_node_to_tree(self.root)

    def _find_path(self, start_node, goal_node, path: list):
        """
        Find path between start_node and goal_node
        """

        if start_node is None or goal_node is None:
            raise ValueError('start_node and goal_node can not be null when trying to find a path from start node to '
                             'goal node')

        path.append(start_node.pos)
        if np.linalg.norm(start_node.pos - goal_node.pos) <= self.seg_len:
            return True
        else:
            for child in start_node.children:
                is_found = self._find_path(child, goal_node, path)
                if is_found:
                    return is_found
                else:
                    path.pop()

            return False


class RRT(RandomTree):
    """
    RRT tree
    """

    def __init__(self, world):
        super().__init__(world)

    def start_planning(self, start_pos, goal_pos):
        super()._init_tree(start_pos)

        curr_node = self.root
        while not self._should_stop(curr_node, goal_pos):
            curr_node = self._extend()

        path = []
        super()._find_path(self.root, curr_node, path)

        # append goal pos
        path.append(goal_pos)

        return path

    def _extend(self):
        """
        Extend tree
        """

        new_node, nearest_node = super()._get_node_to_add(self.seg_len)
        nearest_node.children.append(new_node)
        super()._add_node_to_tree(new_node)

        return new_node

    def _should_stop(self, curr_node, goal_pos):
        return np.linalg.norm(curr_node.pos - np.array(goal_pos)) <= self.seg_len


class KdTree(object):
    """
    Kd tree
    """

    def __init__(self):
        self.root = None

    def add_to_tree(self, node):
        new_root = self._add_to_tree_helper(self.root, node, 0)
        self.root = new_root

    def _add_to_tree_helper(self, root: TreeNode, node: TreeNode, depth: int):
        """
        Add node to Kd tree
        """

        if node is None:
            return root

        curr_dim = depth % 2

        if root is None:
            node.splitDim = curr_dim
            return node

        if node.pos[curr_dim] <= root.pos[curr_dim]:
            root.left = self._add_to_tree_helper(root.left, node, depth + 1)
        else:
            root.right = self._add_to_tree_helper(root.right, node, depth + 1)

        return root

    def nearest_neighbor(self, query_node: TreeNode):
        """
        Find nearest neighbor
        """

        if self.root is None:
            return query_node
        if query_node is None:
            return None

        curr_node = self.root # type: TreeNode
        search_path= []

        self._find_closest_leaf_node(curr_node, query_node, search_path)
        nearest_node = search_path.pop()
        nearest_dist = nearest_node.dist2other(query_node)

        while search_path:
            curr_node = search_path.pop()
            if curr_node.left is None and curr_node.right is None:
                if curr_node.dist2other(query_node) < nearest_dist:
                    nearest_node, nearest_dist = curr_node, curr_node.dist2other(query_node)
            else:
                curr_split_dim = curr_node.splitDim
                if abs(curr_node.pos[curr_split_dim] - query_node.pos[curr_split_dim]) < nearest_dist:
                    if curr_node.dist2other(query_node) < nearest_dist:
                        nearest_node, nearest_dist = curr_node, curr_node.dist2other(query_node)

                    if query_node.pos[curr_split_dim] <= curr_node.pos[curr_split_dim]:
                        curr_node = curr_node.right
                    else:
                        curr_node = curr_node.left

                    self._find_closest_leaf_node(curr_node, query_node,search_path)

        return nearest_node

    def _find_closest_leaf_node(self, curr_node: TreeNode, query_node: TreeNode, search_path: list):
        """
        Find the leaf node that's closest the query node
        """

        if curr_node is None:
            return

        search_path.append(curr_node)
        if curr_node.left is not None and curr_node.right is not None:
            split_dim = curr_node.splitDim
            if query_node.pos[split_dim] <= curr_node.pos[split_dim]:
                curr_node = curr_node.left
            else:
                curr_node = curr_node.right
        else:
            curr_node = curr_node.right if curr_node.left is None else curr_node.left

        self._find_closest_leaf_node(curr_node, query_node, search_path)


class PlanningUtil(object):
    """
    util functions
    """

    @classmethod
    def is_collision(cls, node1: TreeNode, node2: TreeNode, world: World):
        """
        Check collision between nodes
        """

        res = False

        for obs in world.obstacles:
            if cls._is_collision_to_obs(node1, node2, obs):
                res = True
                break

        return res

    @classmethod
    def _is_collision_to_obs(cls, node1: TreeNode, node2: TreeNode, obs):
        """
        Check whether collision with one obstacle
        """

        if obs.is_in_obs(node1.pos) or obs.is_in_obs(node2.pos):
            return True

        if node1.dist2other(node2) < 0.5:
            return False
        else:
            mid_node = TreeNode((node1.pos + node2.pos) * 0.5)
            return cls._is_collision_to_obs(mid_node, node1, obs) or cls._is_collision_to_obs(mid_node, node2, obs)


if __name__ == '__main__':
    start_pos = [0, 0]
    goal_pos = [90, 90]

    path = Planner.plan(start_pos, goal_pos, PlannerType.rrt)

    plotter = Plotter()
    plotter.plot_world(Planner.world)
    plotter.plot_path(path)
    plotter.show()




