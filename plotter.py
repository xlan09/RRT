"""
Plotter
"""

import matplotlib.pyplot as plt


class Plotter(object):
    """
    Plotter
    """

    # class variable to track number of figures generated
    figure = 0

    def __init__(self):
        Plotter.figure += 1
        self.figure = Plotter.figure

    def plot_world(self, world):
        """
        Plot world
        """

        plt.figure(self.figure)
        plt.title('RRT')
        plt.axis([world.sw_corner[0], world.ne_corner[0], world.sw_corner[1], world.ne_corner[1]])

        for obs in world.obstacles:
            plt.fill_between([obs.sw_corner[0], obs.ne_corner[0]], obs.sw_corner[1], obs.ne_corner[1], facecolor='k')

    def plot_tree(self, tree):
        """
        Plot tree
        """

        for node in tree.nodes:
            plt.plot(node.pos[0], node.pos[1], 'bo')

    def plot_path(self, path: list):
        """
        Plot path
        """

        x = [pos[0] for pos in path]
        y = [pos[1] for pos in path]
        plt.figure(self.figure)
        plt.plot(x, y, 'ro-')

    def show(self):
        plt.figure(self.figure)
        plt.show()