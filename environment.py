#!/usr/bin/env python3
"""
Planning environment
"""

import numpy as np
from plotter import Plotter


class Obstacle(object):
    """
    Rectangular obstacles
    """

    def __init__(self, ne_corner, sw_corner):
        if len(ne_corner) != 2 or len(sw_corner) != 2 or ne_corner[0] <= sw_corner[0] or ne_corner[1] <= sw_corner[1]:
            raise ValueError('Not valid corner specifications!')

        self.ne_corner = np.array(ne_corner)
        self.sw_corner = np.array(sw_corner)

    def is_in_obs(self, pos):
        if len(pos) < 2:
            raise ValueError('Not valid arguments when checking whether position is inside obstacle')

        return pos[0] >= self.sw_corner[0] and pos[0] <= self.ne_corner[0] and pos[1] >= self.sw_corner[1] and pos[1] <= self.ne_corner[1]


class World(object):
    """
    Planning environment with fixed number of obstacles, only support 2D environment
    Future extensions can be: making the number of obstacles configurable
    """

    def __init__(self, sw_corner, ne_corner):
        if len(ne_corner) != 2 or len(sw_corner) !=2 or ne_corner[0] <= sw_corner[0] or ne_corner[1] <= sw_corner[1]:
            raise ValueError('Not valid corner specifications!')

        self.ne_corner = np.array(ne_corner)
        self.sw_corner = np.array(sw_corner)
        self.obstacles = []
        self.__init_obs()

    def __init_obs(self):
        """
        Manually choose obstacle positions

        :return:
        """
        base_length= (1.0 / 10) * min(self.ne_corner[0] - self.sw_corner[0], self.ne_corner[1] - self.sw_corner[1])
        obs_sw_corner = np.array(self.sw_corner)

        # Add obstacles
        horizontal_obs = Obstacle(obs_sw_corner + np.array([2 * base_length, base_length]), obs_sw_corner)

        transition = np.array([2 * base_length, 2 * base_length])
        self.obstacles.append(Obstacle(horizontal_obs.ne_corner + transition, horizontal_obs.sw_corner + transition))

        transition = np.array([2 * base_length, 6 * base_length])
        self.obstacles.append(Obstacle(horizontal_obs.ne_corner + transition, horizontal_obs.sw_corner + transition))

        vertical_obs = Obstacle(obs_sw_corner + np.array([base_length, 2 * base_length]), obs_sw_corner)

        transition = np.array([6 * base_length, 1 * base_length])
        self.obstacles.append(Obstacle(vertical_obs.ne_corner + transition, vertical_obs.sw_corner + transition))

        transition = np.array([8 * base_length, 3 * base_length])
        self.obstacles.append(Obstacle(vertical_obs.ne_corner + transition, vertical_obs.sw_corner + transition))

        transition = np.array([6 * base_length, 7 * base_length])
        self.obstacles.append(Obstacle(vertical_obs.ne_corner + transition, vertical_obs.sw_corner + transition))

    def is_free(self, pos):
        if not (pos[0] >= self.sw_corner[0] and pos[0] <= self.ne_corner[0] and pos[1] >= self.sw_corner[1] \
               and pos[1] <= self.ne_corner[1]):
            return False

        for obs in self.obstacles:
            if obs.is_in_obs(pos):
                return False

        return True


if __name__ == '__main__':
    sw_corner = [0, 0]
    ne_corner = [100, 100]
    world = World(sw_corner, ne_corner)
    plotter = Plotter()
    plotter.plot_world(world)
    plotter.show()